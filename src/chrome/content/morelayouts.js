/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is MoreLayoutsforThunderbird.
 *
 * The Initial Developer of the Original Code is
 *   alta88 <alta88@gmail.com>.
 *
 * Portions created by the Initial Developer are Copyright (C) 2018
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

"use strict"

ChromeUtils.import("resource://gre/modules/Services.jsm");

var MoreLayouts = {
  e: function(element) {
    return document.getElementById(element);
  },

  get paneConfig() {
    return Services.prefs.getIntPref("mail.pane_config.dynamic");
  },

  set paneConfig(val) {
    Services.prefs.setIntPref("mail.pane_config.dynamic", val);
  },

  /**
   * The fullscreenPaneConfig pref can set the layout in fullscreen mode:
   * "classic"=0, "widemessage"=1, "vertical"=2, "widethread"=3, "stacked"=4
   * A value of -1 indicates no change from standard size layout.
   */
  get fullscreenPaneConfig() {
    let pref = "extensions.morelayouts.fullscreenPaneConfig";
    if (Services.prefs.prefHasUserValue(pref)) {
      let val = Services.prefs.getIntPref(pref);
      let valid = [this.kStandardPaneConfig,
                   this.kWidePaneConfig,
                   this.kVerticalPaneConfig,
                   this.kWideThreadPaneConfig,
                   this.kStackedPaneConfig,
                   this.kFullscreenPaneConfigDefault].includes(val);
      if (valid)
        return val;
    }

    Services.prefs.setIntPref(pref, this.kFullscreenPaneConfigDefault);
    return this.kFullscreenPaneConfigDefault;
  },

  gCurrentPaneConfig:              kStandardPaneConfig,
  gAccountCentralLoaded:           true,
  kStandardPaneConfig:             0,
  kWidePaneConfig:                 1,
  kVerticalPaneConfig:             2,
  kWideThreadPaneConfig:           3,
  kStackedPaneConfig:              4,
  kAccountCentral:                 5,
  kFullscreenPaneConfigDefault:    -1,

  dynamicViewName:                 ["standard",
                                    "widemessage",
                                    "vertical",
                                    "widethread",
                                    "stacked",
                                    "accountcentral"],

  dynamicViewResetDOMType:         [3, 4],

  dynamicIdsMessagePane:           ["messagesBox",
                                    "mailContent",
                                    "threadPaneBox",
                                    "messagesBox",
                                    "messagesBox"],

  dynamicIdsThreadPane:            ["messagesBox",
                                    "messagesBox",
                                    "messagesBox",
                                    "mailContent",
                                    "folderPaneBox"],

  dynamicIdsThreadPaneSplitter:    ["messagesBox",
                                    "mailContent",
                                    "threadPaneBox",
                                    "mailContent",
                                    "folderPaneBox"],

  // For Orient states, 0=horizontal, 1=vertical.
  dynamicThreadPaneSplitterOrient: [1, 1, 0, 1, 1],

  dynamicMessagePaneBoxFlex:       [2, 1, 2, 2, 2],
  dynamicThreadPaneBoxFlex:        [1, 1, 1, 1, 0],


  /**
   * For F8 toggle. The messagePaneToggleMode pref has 3 values:
   * 0: toggle normal<->min;
   * 1: toggle normal<->max;
   * 2: cycle normal->min->normal->max->normal->min..
   */
  get messagePaneToggleMode() {
    let pref = "extensions.morelayouts.messagePaneToggleMode";
    if (Services.prefs.prefHasUserValue(pref)) {
      let val = Services.prefs.getIntPref(pref);
      let valid = [this.kToggleNormalMin,
                   this.kToggleNormalMax,
                   this.kToggleCycle].includes(val);
      if (valid)
        return val;
    }

    Services.prefs.setIntPref(pref, this.kToggleNormalMin);
    return this.kToggleNormalMin;
  },

  gCurrentMessagePaneConfig:       null,
  gPriorMessagePaneConfig:         null,
  kMessagePaneNormal:              "normal",
  kMessagePaneMin:                 "min",
  kMessagePaneMax:                 "max",
  kToggleNormalMin:                0,
  kToggleNormalMax:                1,
  kToggleCycle:                    2,

  get currentTabInfo() {
    let tabmail = this.e("tabmail");
    return tabmail && tabmail.currentTabInfo ? tabmail.currentTabInfo : null;
  },

  TabMonitor: {
    monitorName: "MoreLayouts",
    onTabTitleChanged: function() {},
    onTabSwitched: function(aTab, aOldTab) {
///console.log("TabMonitor.onTabSwitched: title - "+aTab.title);
      MoreLayouts.updateTabDisplay();
    }
  },

  FolderDisplayListener: {
    onMakeActive: function(aFolderDisplay) {
///console.log("onMakeActive: START ");
      if (aFolderDisplay.view.dbView) {
///console.log("onMakeActive: showThreadPane dbView");
        let selectedPanel = MoreLayouts.e("displayDeck").selectedPanel;
        let threadPaneBox = MoreLayouts.e("threadPaneBox");

        if (selectedPanel == threadPaneBox) {
          // It seems makeActive() fires twice merely switching from account to a
          // folder; don't need to go into updateMailPaneConfig.
          return;
        }
///console.log("onMakeActive: showThreadPane continue ");

        // Set the layout.
        MoreLayouts.gAccountCentralLoaded = false;

        aFolderDisplay.view.dbView.init(messenger, msgWindow,
                                        aFolderDisplay.view.listener.threadPaneCommandUpdater);
      }
      else {
///console.log("onMakeActive: showAccountCentral ");
        // Set the layout, if necessary.  At least print the layout message.
        MoreLayouts.gAccountCentralLoaded = true;
      }

      MoreLayouts.updateMailPaneConfig(!MoreLayouts.gAccountCentralLoaded);
    },

    onActiveCreatedView: function(aFolderDisplay) {
///console.log("onActiveCreatedView: START ");
      MoreLayouts.updateTabDisplay();
    },
  },

  onLoad: function() {
    let descriptionStrPref = "extensions.morelayoutsforthunderbird@mozdev.org.description";
    let descriptionStr =
      Services.strings.createBundle("chrome://morelayouts/locale/morelayouts.properties")
                      .GetStringFromName(descriptionStrPref);
    Services.prefs.setStringPref(descriptionStrPref, descriptionStr);

    FolderDisplayListenerManager.registerListener(MoreLayouts.FolderDisplayListener);
    let tabmail = MoreLayouts.e("tabmail");
    if (tabmail)
      tabmail.registerTabMonitor(MoreLayouts.TabMonitor);
  },

  /**
   * Restore the msgWindow object.
   */
  rerootMsgWindow: function() {
///console.log("rerootMsgWindow: START ");

    // This would be simpler to fix in c++ but. This is to importantly null
    // mMessageWindowDocShellWeak, but it takes out other stuff so a new
    // msgWindow will be necessary.
    msgWindow.closeWindow();
    // Mostly to create a new msgWindow with Init() setting up nsITransactionManager
    // (undo/redo) and nsIURILoader properly.
    CreateMailWindowGlobals();
    // From LoadPostAccountWizard(). Re-init the new msgWindow and docShells.
    InitMsgWindow();

    messenger.setWindow(null, null);
    messenger.setWindow(window, msgWindow);

    // And now the current dbview; re-init mMsgWindowWeak etc.
    gFolderDisplay.msgWindow = msgWindow;
    gFolderDisplay.messenger = messenger;
    gFolderDisplay.view.dbView.init(messenger, msgWindow,
                                    gFolderDisplay.view.listener.threadPaneCommandUpdater);

    msgWindow.statusFeedback.showStatusString(" ");

    // Now make sure each tab's msgWindow object is reset.
    let tabmail = this.e("tabmail") || [];
    for (let tab of tabmail.tabInfo) {
      if (tab.mode.type == "folder" || tab.mode.name == "glodaList")
        tab.folderDisplay.messenger.setWindow(window, msgWindow);
      if (tab.mode.type == "message" || tab.mode.name == "glodaList")
        tab.messageDisplay.folderDisplay.messenger.setWindow(window, msgWindow);
    }

///console.log("rerootMsgWindow: FINISHED ");
  },

  /**
   * Restore threadpane references and view.
   */
  rerootThreadPane: function() {
///console.log("rerootThreadPane: START ");
    if (!gDBView)
      return;

    let tabmail = this.e("tabmail");
    let threadTree = GetThreadTree();
    let treeView = gFolderDisplay.view.dbView;

    for (let tabInfo of tabmail.tabInfo) {
      if (tabInfo.mode.type == "folder" || tabInfo.mode.name == "glodaList") {
        tabInfo.folderDisplay.tree = threadTree;
        tabInfo.folderDisplay.treeBox = threadTree.boxObject; //.QueryInterface(Components.interfaces.nsITreeBoxObject);
///console.log("rerootThreadPane: tab type:title - "+tabInfo.mode.type+":"+tabInfo.title);
      }
    }

    if (treeView) {
      threadTree.boxObject.view = treeView;
///console.log("rerootThreadPane: treeView completed");
    }
///console.log("rerootThreadPane: FINISHED ");
  },

  /**
   * When switching tabs and showing threadpane, make sure all views are
   * correct, given we may be in a message pane toggle state. Each folder tab
   * may have its own state.
   */
  updateTabDisplay: function() {
///console.log("updateTabDisplay: START ");
    if (!this.currentTabInfo)
      return;

    let tabMode = this.currentTabInfo.mode;
///console.log("updateTabDisplay: mode type:name - "+tabMode.type+":"+tabMode.name);

    this.e("mailContent").setAttribute("tabmodetype", tabMode.type);

    if (tabMode.type == "folder") {
      let folderDisplay = this.currentTabInfo.folderDisplay;
      if ("messagePaneState" in folderDisplay)
        this.gCurrentMessagePaneConfig = folderDisplay.messagePaneState;
      else
        this.gCurrentMessagePaneConfig = this.kMessagePaneNormal;

      this.MessagePaneMaxShowHide(this.gCurrentMessagePaneConfig);
    }
    else if (tabMode.name == "glodaList" &&
             this.gCurrentPaneConfig == this.kStackedPaneConfig) {
      // Ensure threadpane is visible in glodaList stacked view.
      this.updateStackedGlodaList();
    }
  },

  /**
   * Set enable/disable status of View->Layout and AppMenu->..->Layout
   * menuitems.
   */
  InitViewLayoutStyleMenu: function(aEvent) {
    aEvent.stopImmediatePropagation();

    let layoutItems = this.e("view_layout_popup")
                          .querySelectorAll("[name='viewlayoutgroup']");
    let enable = this.currentTabInfo &&
                 this.currentTabInfo.mode.type == "folder" &&
                 this.gCurrentMessagePaneConfig == this.kMessagePaneNormal;

    for (let item of layoutItems) {
      if (this[item.getAttribute("paneconfig")] == this.gCurrentPaneConfig)
        item.setAttribute("checked", "true");
///console.log("InitViewLayoutStyleMenu: START itemId:enable:checked - "+
///            item.id+":"+enable+":"+item.getAttribute("checked"));

      if (enable)
        item.removeAttribute("disabled");
      else
        item.setAttribute("disabled", true);
    }

    this.InitViewLayoutReverseMenu();
  },

  /**
   * Set enable/disable status of View->Layout and AppMenu->..->Layout
   * reverse menuitems.
   */
  InitViewLayoutReverseMenu: function() {
    let layoutItems = this.e("view_layout_popup")
                          .querySelectorAll("[reverselayout]");
    let enable = this.currentTabInfo &&
                 this.currentTabInfo.mode.type == "folder" &&
                 this.gCurrentMessagePaneConfig == this.kMessagePaneNormal;

    for (let item of layoutItems) {
      let disable = this[item.getAttribute("paneconfig")] != this.gCurrentPaneConfig;
///console.log("InitViewLayoutReverseMenu: START itemId:enable:disable - "+
///            item.id+":"+enable+":"+disable);

      if (enable && !disable)
        item.removeAttribute("disabled");
      else
        item.setAttribute("disabled", true);
    }
  },

  /**
   * Replace UpdateMailPaneConfig().
   */
  updateMailPaneConfig: function(aMsgWindowInitialized, aConfig) {
    let paneConfig = aConfig == null ? this.paneConfig : aConfig;

///console.log("updateMailPaneConfig: init:paneConfig:gCurrentPaneConfig:gAccountCentralLoaded " +
///            aMsgWindowInitialized+":"+paneConfig+":"+
///            this.gCurrentPaneConfig+":"+this.gAccountCentralLoaded);

    // Set to standard/classic if in accountcentral and widethread or stacked
    // layout, so accountcentral displays correctly.
    if (this.gAccountCentralLoaded &&
        this.dynamicViewResetDOMType.includes(paneConfig))
      paneConfig = kStandardPaneConfig;

    // Make sure it's valid.
    paneConfig = this.dynamicIdsMessagePane[paneConfig] ? paneConfig :
                                                          kStandardPaneConfig;

    // Set view name for css selectors.
    let viewName = this.gAccountCentralLoaded ?
                     this.dynamicViewName[this.kAccountCentral] :
                     this.dynamicViewName[paneConfig];
    this.e("mailContent").setAttribute("layout", viewName);

    if (!aMsgWindowInitialized) {
      // Some init stuff.
      this.gCurrentMessagePaneConfig = this.kMessagePaneNormal;
      this.gPriorMessagePaneConfig = this.kMessagePaneMax;
    }

    console.info("updateMailPaneConfig: layout --> " + viewName);

    if (paneConfig == this.gCurrentPaneConfig || !gFolderDisplay)
      // No change required.
      return;

    let rerootThreadPane = false;
    let desiredIdMessagePane = this.dynamicIdsMessagePane[paneConfig];
    let desiredIdThreadPane = this.dynamicIdsThreadPane[paneConfig];
    let desiredIdThreadPaneSplitter = this.dynamicIdsThreadPaneSplitter[paneConfig];

    // threadpane+acct central.
    let displayDeck = this.e("displayDeck");
    // GetMessagePane();
    gMessagePane = this.e("messagepanebox");
    // GetMessagePaneBoxWrapper();
    gMessagePaneWrapper = this.e("messagepaneboxwrapper");
    // GetThreadAndMessagePaneSplitter();
    gThreadAndMessagePaneSplitter = this.e("threadpane-splitter");

///console.log("updateMailPaneConfig: messagePaneBoxWrapper - parent:desiredParent - " +
///            gMessagePaneWrapper.parentNode.id+":"+desiredIdMessagePane);

    let footerBox = this.e("msg-footer-notification-box");
    let cloneFooterBox;
    if (footerBox) {
      footerBox.removeTransientNotifications();
      cloneFooterBox = footerBox.cloneNode(true);
      footerBox.remove();
    }

    if (gMessagePaneWrapper.parentNode.id != desiredIdMessagePane) {

      // Only for non multimessage view.
      if (gMessageDisplay.singleMessageDisplay)
        ClearAttachmentList();

      let hdrToolbox = this.e("header-view-toolbox");
      let hdrToolbar = this.e("header-view-toolbar");
      let firstPermanentChild = hdrToolbar.firstPermanentChild;
      let lastPermanentChild = hdrToolbar.lastPermanentChild;
      let desiredParent = this.e(desiredIdMessagePane);

      // "Here the message pane including the header pane is moved..."
      let cloneToolboxPalette;
      let cloneToolbarset;
      if (hdrToolbox.palette) {
        cloneToolboxPalette = hdrToolbox.palette.cloneNode(true);
      }
      if (hdrToolbox.toolbarset) {
        cloneToolbarset = hdrToolbox.toolbarset.cloneNode(true);
      }

      // "See Bug 381992. The ctor for the browser element will fire again..."
      this.e("messagepane").destroy();

      desiredParent.appendChild(gThreadAndMessagePaneSplitter);
      desiredParent.appendChild(gMessagePaneWrapper);

      hdrToolbox.palette = cloneToolboxPalette;
      hdrToolbox.toolbarset = cloneToolbarset;
      hdrToolbar = this.e("header-view-toolbar");
      hdrToolbar.firstPermanentChild = firstPermanentChild;
      hdrToolbar.lastPermanentChild = lastPermanentChild;
    }

///console.log("updateMailPaneConfig: displayDeck - parent:desiredParent - " +
///            displayDeck.parentNode.id+":"+desiredIdThreadPane);
///console.log("updateMailPaneConfig: threadpane-splitter - parent:desiredParent - " +
///            gThreadAndMessagePaneSplitter.parentNode.id+":"+
///            desiredIdThreadPaneSplitter);

    if (displayDeck.parentNode.id != desiredIdThreadPane) {
      let desiredParent = this.e(desiredIdThreadPane);
      let desiredParentSplitter = this.e(desiredIdThreadPaneSplitter);
      switch (paneConfig) {
        case kStandardPaneConfig: // 0
        case this.kWideThreadPaneConfig: // 3
          desiredParent.insertBefore(displayDeck, desiredParent.lastChild);
          break;
        case kWidePaneConfig: // 1
        case kVerticalPaneConfig: // 2
        case this.kStackedPaneConfig: // 4
          desiredParent.appendChild(displayDeck);
          break;
      }

      // Make sure splitter is in the right place.
      desiredParentSplitter.insertBefore(gThreadAndMessagePaneSplitter,
                                         desiredParentSplitter.lastChild);

      rerootThreadPane = true;
    }

    if (cloneFooterBox)
      this.e("mailContent").appendChild(cloneFooterBox);

    // Set element orient states for a given view.
    gThreadAndMessagePaneSplitter.orient =
      this.dynamicThreadPaneSplitterOrient[paneConfig] ? "vertical" : "horizontal";

    // Make sure dimensions always maintained as set.  Also set flex on a tab
    // change for non 3pane.
    gMessagePaneWrapper.setAttribute("flex", this.dynamicMessagePaneBoxFlex[paneConfig]);
    displayDeck.setAttribute("flex", this.dynamicThreadPaneBoxFlex[paneConfig]);

    // Record the new configuration, set the pref in case changed manually
    // (unless we're switching layout for fullscreen).
    this.gCurrentPaneConfig = paneConfig;
    if (!this.gAccountCentralLoaded && !aConfig)
      this.paneConfig = paneConfig;

    this.InitViewLayoutReverseMenu();

    // Reset msgWindow after move.
    if (aMsgWindowInitialized) {
      this.rerootMsgWindow();

      if (rerootThreadPane)
        // Also reset threadpane references after move, if necessary.
        this.rerootThreadPane();

      // For some layout changes, the doc in messagepane or multimessage is
      // not destroyed, so don't create it again.
      let browser = getBrowser();
      if (browser && browser.contentDocument &&
          browser.contentDocument.URL == "about:blank") {
///console.log("updateMailPaneConfig: about:blank ");
        if (!gMessageDisplay.singleMessageDisplay)
          gSummaryFrameManager.pendingOrLoadedUrl = "about:blank";
        gMessageDisplay.makeInactive();
        setTimeout(() => {
          gFolderDisplay.makeActive();
        }, 0);
      }
    }

    // "The quick filter bar gets badly lied to due...."
    setTimeout(function UpdateMailPaneConfig_deferredFixup() {
      let threadPaneBox = document.getElementById("threadPaneBox");
      let overflowNodes = threadPaneBox.querySelectorAll("[onoverflow]");
      for (let iNode = 0; iNode < overflowNodes.length; iNode++) {
        let node = overflowNodes[iNode];
        if (node.scrollWidth > node.clientWidth) {
          let e = document.createEvent("HTMLEvents");
          e.initEvent("overflow", false, false);
          node.dispatchEvent(e);
        }
        else if (node.onresize) {
          let e = document.createEvent("HTMLEvents");
          e.initEvent("resize", false, false);
          node.dispatchEvent(e);
        }
      }
    }, 1500);

  },

  /**
   * Grrr.
   */
  updateStackedGlodaList: function() {
    this.e("folderPaneBox").collapsed = false;
    this.e("folderpane_splitter").collapsed = false;
    this.e("folderpane_splitter").setAttribute("state", "open");
    this.e("displayDeck").collapsed = false;
    this.e("displayDeck").setAttribute("flex", "1");
    this.e("threadpane-splitter").setAttribute("collapse", "before");
    this.e("threadpane-splitter").collapsed = true;
    this.e("threadpane-splitter").setAttribute("state", "collapsed");
  },

  /**
   * Set splitter collapse states to effect the passed message pane state parm.
   *
   * @param aState, string in "normal", "min", "max".
   */
  MessagePaneMaxShowHide: function(aState) {
///console.log("MessagePaneMaxShowHide: mode:current:prior:setState - " +
///            this.messagePaneToggleMode+":"+
///            this.gCurrentMessagePaneConfig+":"+
///            this.gPriorMessagePaneConfig+":"+aState);

    let flex;
    let widethread = this.gCurrentPaneConfig == this.kWideThreadPaneConfig;
    let stacked = this.gCurrentPaneConfig == this.kStackedPaneConfig;
    let threadSplitter = this.e("threadpane-splitter");
    let folderSplitter = this.e("folderpane_splitter");
    let messagepanebox = this.e("messagepanebox");
    let messagepaneboxwrapper = this.e("messagepaneboxwrapper");
    let folderPaneBox = this.e("folderPaneBox");

    if (aState == this.kMessagePaneNormal) {
      if (widethread || stacked)
        folderPaneBox.removeAttribute("flex");

      this.e("threadContentArea").removeAttribute("collapsed");
      threadSplitter.setAttribute("collapse", "after");
      threadSplitter.setAttribute("state", "open");

      folderSplitter.setAttribute("collapse", "before");
      folderSplitter.setAttribute("state", "open");

      let flex = this.dynamicMessagePaneBoxFlex[this.gCurrentPaneConfig];
      messagepaneboxwrapper.setAttribute("flex", flex);
    }
    else if (aState == this.kMessagePaneMin) {
      if (widethread || stacked) {
        threadSplitter.setAttribute("state", "open");

        folderSplitter.setAttribute("collapse", "after");
        folderSplitter.setAttribute("state", "collapsed");
        folderPaneBox.setAttribute("flex", 1);
      }
      else {
        folderSplitter.setAttribute("state", "open");
        threadSplitter.setAttribute("state", "collapsed");
      }
    }
    else if (aState == this.kMessagePaneMax) {
      if (GetNumSelectedMessages() < 1)
        return false;

      threadSplitter.removeAttribute("substate");
      threadSplitter.setAttribute("collapse", "before");
      threadSplitter.setAttribute("state", "collapsed");

      folderSplitter.removeAttribute("substate");
      folderSplitter.setAttribute("collapse", "before");
      folderSplitter.setAttribute("state", "collapsed");

      flex = this.dynamicMessagePaneBoxFlex[kStandardPaneConfig];
      messagepaneboxwrapper.setAttribute("flex", flex);
    }

    messagepanebox.setAttribute("togglestate", aState);
    return true;
  },

  /**
   * An F8 three way max-normal-min message pane toggle function.
   */
  MsgToggleMessagePane: function() {
    if (!this.currentTabInfo || this.currentTabInfo.mode.type != "folder")
      // Bail without doing anything if we are not a folder tab.  Not enabled
      // for glodaList.
      return;

    let setState;
    let currentTabFolderDisplay = null;
    let currentMessagePaneConfig = this.gCurrentMessagePaneConfig;
    let priorMessagePaneConfig = this.gPriorMessagePaneConfig;

    if (this.currentTabInfo.folderDisplay) {
      currentTabFolderDisplay = this.currentTabInfo.folderDisplay;
      if (currentTabFolderDisplay.messagePaneStatePrior)
        priorMessagePaneConfig = currentTabFolderDisplay.messagePaneStatePrior;
    }

    if (currentMessagePaneConfig == this.kMessagePaneMin ||
        currentMessagePaneConfig == this.kMessagePaneMax)
      setState = this.kMessagePaneNormal;
    else if (this.messagePaneToggleMode == this.kToggleCycle)
      setState = priorMessagePaneConfig == this.kMessagePaneMax ?
                   this.kMessagePaneMin : this.kMessagePaneMax;
    else
      setState = this.messagePaneToggleMode == this.kToggleNormalMin ?
                   this.kMessagePaneMin : this.kMessagePaneMax;

    if (!this.MessagePaneMaxShowHide(setState))
      return;

    this.gPriorMessagePaneConfig = this.gCurrentMessagePaneConfig;
    this.gCurrentMessagePaneConfig = setState;
    if (currentTabFolderDisplay) {
      currentTabFolderDisplay.messagePaneStatePrior = currentTabFolderDisplay.messagePaneState;
      currentTabFolderDisplay.messagePaneState = setState;
    }

    ChangeMessagePaneVisibility(IsMessagePaneCollapsed());
    SetFocusThreadPaneIfNotOnMessagePane();
  },

  /**
   * An F11 fullscreen function.
   */
  BrowserFullScreen: function() {
///console.log("BrowserFullScreen: "+window.fullScreen);
    let fullscreen = window.fullScreen;
    let desiredPaneConfig;
    let fullscreenPaneConfig = this.fullscreenPaneConfig;
    if (!fullscreen && fullscreenPaneConfig != -1 &&
        this.dynamicIdsMessagePane[fullscreenPaneConfig])
      desiredPaneConfig = fullscreenPaneConfig;
    else
      desiredPaneConfig = this.paneConfig;

    if (desiredPaneConfig != this.gCurrentPaneConfig) {
      this.updateMailPaneConfig(true, desiredPaneConfig);
///console.log("BrowserFullScreen: done updateMailPaneConfig - "+this.gCurrentPaneConfig);
    }

    this.e("mail-toolbox").setAttribute("collapsed", !fullscreen);
    this.e("status-bar").setAttribute("collapsed", !fullscreen);
    this.e("menu_fullScreen_ML").setAttribute("checked", !fullScreen);

    // If not in fullscreen, set to fullscreen, and vice versa.
    window.fullScreen = !fullScreen;

    if (this.currentTabInfo && this.currentTabInfo.mode.type == "folder")
      this.MessagePaneMaxShowHide(this.gCurrentMessagePaneConfig);
  }
};

/* Override Tb native functions ***********************************************/

/* eslint-disable-next-line no-unused-vars */
function UpdateMailPaneConfig(aMsgWindowInitialized) {
  MoreLayouts.updateMailPaneConfig(aMsgWindowInitialized);
}

/* eslint-disable-next-line no-unused-vars */
function InitViewLayoutStyleMenu(aEvent) {
  MoreLayouts.InitViewLayoutStyleMenu(aEvent);
}

/* eslint-disable-next-line no-unused-vars */
function MsgToggleMessagePane() {
  MoreLayouts.MsgToggleMessagePane();
}

function IsMessagePaneCollapsed() {
  if (MoreLayouts.gCurrentMessagePaneConfig == MoreLayouts.kMessagePaneMax)
    return false;

  if (MoreLayouts.gCurrentPaneConfig == MoreLayouts.kWideThreadPaneConfig ||
      MoreLayouts.gCurrentPaneConfig == MoreLayouts.kStackedPaneConfig)
    return document.getElementById("folderpane_splitter")
                   .getAttribute("state") == "collapsed";

  return document.getElementById("threadpane-splitter")
                 .getAttribute("state") == "collapsed";
}

window.addEventListener("load", MoreLayouts.onLoad, false);
