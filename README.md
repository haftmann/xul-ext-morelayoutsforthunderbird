
Pragmatic debian packaging for MoreLayouts for Thunderbird
==========================================================


See also
--------

http://morelayoutsforthunderbird.mozdev.org/


Building
--------

Vanilla debian build:

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git checkout upstream
    $ wget https://bitbucket.org/alta8888/morelayouts/get/MoreLayouts_4.1.tar.bz2
    $ tar -xjvf MoreLayouts_4.1.tar.bz2
    $ rm -rf MoreLayouts_4.1.tar.bz2 src update.rdf updateInfo.xml
    $ mv alta8888-morelayouts-405f056b9d56/* .
    $ rm -rf alta8888-morelayouts-405f056b9d56
    $ git commit -a
